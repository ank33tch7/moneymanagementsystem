﻿using DAL;
using DAL.Persistance.SqlConnect;
using DAL.Persistance.UOW;
using DAL.Services;
using MMSApi.Helpers;
using MMSApi.Services;
using MMSApi.ServicesAndMiddlewares;

namespace MMSApi
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public void ConfigureService(IServiceCollection services)
        {
            services.AddControllersWithViews();

            services.AddSpaStaticFiles(options =>
            {
                options.RootPath = "ClientApp/dist";
            });

            services.AddHttpContextAccessor();
            services.AddDALDependency(_configuration);

            services.AddSwaggerConfigureServices();
            services.AddAuthenticationConfigureService(_configuration);

            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<ISQLHelper, SQLHelper>();
            services.AddSingleton<ICurrentUserService, CurrentUserService>();
            services.AddSingleton<ITokenManager, TokenManager>();
        }

        public void Configure(WebApplication app)
        {
            // global exception handling
            app.Use(async (context, next) =>
            {
                try
                {
                    await next();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    context.Response.StatusCode = 500;
                    await context.Response.WriteAsync("Internal Server Error");
                }
            });

            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();
            app.UseCors(cors =>
            {
                cors.AllowAnyHeader()
                    .AllowAnyMethod()
                    .AllowAnyOrigin();
            });

            app.UseSwaggerConfigure();

            app.UseAuthentication();

            app.UseAuthorization();

          



            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(name: "default",
                pattern: "{controller}/{action=Index}/{id?}");
            });

            //app.Map("api/{**slug}", context =>
            //{
            //    context.Response.StatusCode = StatusCodes.Status404NotFound;
            //    return Task.CompletedTask;
            //});


            //app.MapFallbackToFile("index.html");

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";
                if (app.Environment.IsDevelopment())
                {
                    spa.UseProxyToSpaDevelopmentServer("http://localhost:7071/");
                }
            });

            app.Run();

        }
    }
}
