﻿using DAL.Common;
using DAL.Services;
using System.Reflection.Metadata;
using System.Security.Claims;

namespace MMSApi.Services
{
    public class CurrentUserService : ICurrentUserService
    {
        private readonly IHttpContextAccessor _contextAccessor;
        public CurrentUserService(IHttpContextAccessor contextAccessor)
        {
            _contextAccessor = contextAccessor;
        }

        public string UserId
        {
            get
            {
                return _contextAccessor.HttpContext?.User?.FindFirstValue(JwtTokenContants.UserId);
            }
        }

        public string UserName
        {
            get
            {
                return _contextAccessor.HttpContext?.User?.FindFirstValue(JwtTokenContants.UserName);
            }
        }

        public string FullName
        {
            get
            {
                return _contextAccessor.HttpContext?.User?.FindFirstValue(JwtTokenContants.FullName);
            }
        }

        public string Phone
        {
            get
            {
                return _contextAccessor.HttpContext?.User?.FindFirstValue(JwtTokenContants.Phone);
            }
        }

        public string Email
        {
            get
            {
                return _contextAccessor.HttpContext?.User?.FindFirstValue(JwtTokenContants.Email);
            }
        }
    }
}
