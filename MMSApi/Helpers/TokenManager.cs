﻿using DAL.Common;
using DAL.Models.AdminTable;
using DAL.Services;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace MMSApi.Helpers
{
    public class TokenManager : ITokenManager
    {
        private readonly IConfiguration _configuration;
        public TokenManager(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public JwtSecurityToken GetToken(TokenModel user)
        {
            return GenerateToken(GetClaims(user));
        }
        private List<Claim> GetClaims(TokenModel identityUser)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtTokenContants.UserId, identityUser.UserId),
                new Claim(JwtTokenContants.FullName, identityUser.FullName),
                new Claim(JwtTokenContants.UserName, identityUser.UserName),
                new Claim(JwtTokenContants.Phone, identityUser.Phone),
                new Claim(JwtTokenContants.Email, identityUser.Email),
            };
            return claims;
        }
        private JwtSecurityToken GenerateToken(List<Claim> userClaims)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["tokenDetail:key"]));

            return new JwtSecurityToken(issuer: _configuration["tokenDetail:issuer"],
                                                           audience: _configuration["tokenDetail:audience"],
                                                           claims: userClaims,
                                                           expires: DateTime.UtcNow.AddMinutes(Convert.ToDouble(_configuration["tokenDetail:validMinutes"])),
                                                           signingCredentials: new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256));
        }
    }
}
