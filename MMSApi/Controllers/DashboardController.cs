﻿using DAL.Models.Loan;
using DAL.Models.MTransaction;
using DAL.Persistance.UOW;
using Microsoft.AspNetCore.Mvc;

namespace MMSApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DashboardController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        public DashboardController(IUnitOfWork unitOfWork)
        {
                _unitOfWork = unitOfWork;
        }

        

        [HttpGet("get-income-expense-comparism")]
        public async Task<ActionResult<List<IncomeExpenseComparismVm>>> GetIncomeExpenseComparism()
        {
            var result = await _unitOfWork.Transactions.IncomeExpenseComparism();
            return Ok(result);
        }
        [HttpGet("total-transaction-amount")]
        public async Task<ActionResult<TotalTransactionAmount>> GetTotalTransactionsDetail()
        {
            var expenseAmount = await _unitOfWork.Expenses.GetExpenseAmount();
            var loanAmt = await _unitOfWork.Loans.GetLoanAmount();
            var totalIncome = Math.Round(await _unitOfWork.Incomes.GetTotalIncomeAmount(),2);
            var totalInvestment =Math.Round(await _unitOfWork.Investments.GetTotalInvestmentAmount(),2);
          
            var balanceAmount = (totalIncome + loanAmt.UnPaidLoanTakenAmount + loanAmt.PaidLoanGivenAmount) - (expenseAmount.ExpenseAmount + totalInvestment + expenseAmount.SentHomeAmount + loanAmt.PaidLoanTakenAmount + loanAmt.UnPaidLoanGivenAmount);
            //var actualAmt = balanceAmount - loanAmt.PaidLoanTakenAmount - loanAmt.UnPaidLoanGivenAmount;

            //var balanceAmount = amt < 0 ? 0 : amt;
            //var result = await _unitOfWork.Transactions.TotalTransactionsDetail();
           
            return Ok(new TotalTransactionAmount
            {
                TotalExpense = Math.Round(expenseAmount.ExpenseAmount,2),
                TotalIncome = totalIncome,
                TotalLoanTaken = Math.Round(loanAmt.TotalLoanTakenAmount,2),
                TotalLoanGiven = Math.Round(loanAmt.TotalLoanGivenAmount,2),
                TotalInvestment = totalInvestment,
                TotalAmountSent = Math.Round(expenseAmount.SentHomeAmount, 2),
                BalanceAmount = balanceAmount,
                //ActualAmount = actualAmt,
            });
        }

        [HttpGet("get-expense-by-category")]
        public async Task<ActionResult<List<IncomeExpenseComparismVm>>> GetExpenseByCategory()
        {
            var result = await _unitOfWork.Expenses.GetExpenseByCategories();
            return Ok(result);
        }


        [HttpGet("loan-report-detail")]
        public async Task<ActionResult<LoanReportVm>> LoanReportDetail()
        {
            var result = await _unitOfWork.Loans.LoanReportDetail();
            return Ok(result);
        }
    }
}
