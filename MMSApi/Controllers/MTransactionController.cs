﻿using DAL.Common;
using DAL.Models;
using DAL.Models.Expense;
using DAL.Models.Income;
using DAL.Models.Investment;
using DAL.Models.Loan;
using DAL.Models.MTransaction;
using DAL.Persistance.UOW;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MMSApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MTransactionController : ControllerBase    
    {
        private readonly IUnitOfWork _unitOfWork;
        public MTransactionController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpPost("create-transaction")]
        public async Task<IActionResult> CreateTransaction([FromBody] CreateTransactionCommand transaction)
        {
            var result = await _unitOfWork.Transactions.CreateTransaction(transaction);
            if (result.Code != 0)
                return BadRequest(result);
            return Ok(result);
        }

        [HttpPost("update-transaction")]
        public async Task<IActionResult> UpdateTransaction([FromBody] UpdateTansactionCommand transaction)
        {
            var result = await _unitOfWork.Transactions.UpdateTransaction(transaction);
            if (result.Code != 0)
                return BadRequest(result);
            return Ok(result);
        }


        [HttpGet("get-incomes")]
        public async Task<ActionResult<List<IncomeVm>>> GetIncomes()
        {
            var result = await _unitOfWork.Incomes.GetIncomes();
            return Ok(result);
        }

        [HttpGet("get-expenses")]
        public async Task<ActionResult<List<ExpenseVm>>> GetExpenses()
        {
            var result = await _unitOfWork.Expenses.GetExpenses();
            return Ok(result);
        }

        [HttpGet("get-investment")]
        public async Task<ActionResult<List<InvestmentVm>>> GetInvestments()
        {
            var result = await _unitOfWork.Investments.GetInvestments();
            return Ok(result);
        }



        [HttpGet("get-loan")]
        public async Task<ActionResult<List<LoanVm>>> GetLoans()
        {
            var result = await _unitOfWork.Loans.GetLoans();
            return Ok(result);
        }

        [HttpGet("get-loan-by-type/{type}")]
        public async Task<ActionResult<List<LoanVm>>> GetLoanByType([FromRoute] LoanType type)
        {
            var result = await _unitOfWork.Loans.GetLoanByType(type);
            return Ok(result);
        }
       

        [HttpPut("update-loan-paid/{id}")]
        public async Task<ActionResult<DbResponse>> UpdateLoanPaid([FromRoute]Guid id)
        {
            var result = await _unitOfWork.Loans.SetAsLoanPaid(id);
            if(result.Code != 0)
                return BadRequest(result);
            return Ok(result);
        }






        [HttpDelete("delete-transaction/{id}/{transactionType}")]
        public async Task<IActionResult> DeleteTransaction([FromRoute] Guid id, [FromRoute] TransactionType transactionType)
        {
            var result = await _unitOfWork.Transactions.DeleteTransaction(transactionType, id);
            return Ok(result);
        }

    }
}
