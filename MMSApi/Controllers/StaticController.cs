﻿using DAL.Common;
using DAL.Models;
using DAL.Models.StaticValues;
using DAL.Persistance.UOW;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace MMSApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class StaticController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        public StaticController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;  
        }

        [HttpGet("get-static-value-type")]
        public async Task<ActionResult<List<StaticValueTypeVm>>> GetStaticValueTypes()
        {
            var staticValueTypes = await _unitOfWork.Static.GetStaticValueTypes();
            return Ok(staticValueTypes);
        }
        [HttpGet("get-static-value-type-for-dropdown")]
        public async Task<ActionResult<List<StaticValueTypeForDropdownVm>>> GetStaticValueTypesForDropDown()
        {
            var staticValueTypes = await _unitOfWork.Static.GetStaticValueTypeForDropdown();
            return Ok(staticValueTypes);
        }

        [HttpPost("create-static-value-type")]
        public async Task<ActionResult> CreateStaticValueType([FromBody] CreateStaticValueTypeCommand command)
        {
            var result = await _unitOfWork.Static.CreateStaticValueType(command);
            return Ok(result);
        }

        [HttpPut("update-static-value-type")]
        public async Task<ActionResult> UpdateStaticValueType([FromBody] UpdateStaticValueTypeCommand command)
        {
            var result = await _unitOfWork.Static.UpdateStaticValueType(command);
            return Ok(result);
        }
        [HttpDelete("delete-static-value-type/{id}")]
        public async Task<ActionResult> DeleteStaticValueType([FromRoute] int id)
        {
            var result = await _unitOfWork.Static.DeleteStaticValueType(id);
            return Ok(result);
        }

        [HttpGet("get-static-value")]
        public async Task<ActionResult<List<StaticValueVm>>> GetStaticValues()
        {
            var staticValueTypes = await _unitOfWork.Static.GetStaticValues();
            return Ok(staticValueTypes);
        }

        [HttpGet("get-static-value/{typeId}")]
        public async Task<ActionResult<List<StaticValueVm>>> GetStaticValuesByTypeId(int typeId)
        {
            var staticValueTypes = await _unitOfWork.Static.GetStaticValuesByTypeId(typeId);
            return Ok(staticValueTypes);
        }

        [HttpPost("create-static-value")]
        public async Task<ActionResult> CreateStaticValue([FromBody] CreateStaticValueCommand command)
        {
            var result = await _unitOfWork.Static.CreateStaticValue(command);
            return Ok(result);
        }
        [HttpPost("update-static-value")]
        public async Task<ActionResult> UpdateStaticValuee([FromBody] UpdateStaticValueCommand command)
        {
            var result = await _unitOfWork.Static.UpdateStaticValue(command);
            return Ok(result);
        }

        [HttpDelete("delete-static-value/{id}")]
        public async Task<ActionResult> DeleteStaticValue([FromRoute] int id)
        {
            var result = await _unitOfWork.Static.DeleteStaticValue(id);
            return Ok(result);
        }

        // get dropdown

        [HttpGet("get-dropdown-for-category/{transactionType}")]
        public async Task<ActionResult<List<DropDownModel>>> GetDropDownForCategory([FromRoute]  TransactionType transactionType)
        {
            var result =  await _unitOfWork.Static.GetCategoryByType(transactionType);
            return Ok(result);
        }

        
    }
}
