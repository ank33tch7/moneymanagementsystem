﻿using DAL.Models.AdminTable;
using DAL.Persistance.UOW;
using DAL.Services;
using Microsoft.AspNetCore.Mvc;
using System.IdentityModel.Tokens.Jwt;

namespace MMSApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITokenManager _tokenManager;
        public AccountController(IUnitOfWork unitOfWork, ITokenManager tokenManager)
        {
            _unitOfWork = unitOfWork;
            _tokenManager = tokenManager;
        }

        [HttpPost("create-user")]
        public async Task<ActionResult> CreateUser([FromBody]CreateUserCommand command)
        {
            var result = await _unitOfWork.Users.CreateUser(command);
            return Ok(result);
        }

        [HttpPost("authenticate-user")]
        public async Task<ActionResult<TokenResponse>> AuthenticateUser([FromBody] AuthenticateRequest request)
        {
            var response = await _unitOfWork.Users.AuthenticateUser(request);
            if(response.Code != 200)
                return Unauthorized(new TokenResponse
                {
                    Code = response.Code,
                    Message = response.Message,
                });

            var securityToken = _tokenManager.GetToken(response.TokenModel);

            return Ok(new
            {
                Code = 200,
                Message = "User Logged in successfully.",
                Token = new JwtSecurityTokenHandler().WriteToken(securityToken),
            });
        }
    }
}
