﻿using DAL.Persistance.DataContext;
using DAL.Persistance.UOW;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DAL
{
    public static class DALDependency
    {
        public static IServiceCollection AddDALDependency(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"), options =>
                {
                    options.CommandTimeout(200);
                });
            });


            return services;
        }
    }
}
