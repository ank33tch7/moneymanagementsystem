﻿using DAL.Common;

namespace DAL.Models.Loan
{
    public class CreateLoanCommand
    {
        public string Name { get; set; }
        public decimal InterestRate { get; set; }
        public LoanType LoanType { get; set; }
        public bool IsPaid { get; set; }
        public string Notes { get; set; }
        public DateTime LoanProvidedDate { get; set; }
        public DateTime? ReypaymentDate { get; set; }
    }
    public class UpdateLoanCommand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal InterestRate { get; set; }
        public LoanType LoanType { get; set; }
        public bool IsPaid { get; set; }
        public string Notes { get; set; }
        public DateTime LoanProvidedDate { get; set; }
        public DateTime? ReypaymentDate { get; set; }
    }
}
