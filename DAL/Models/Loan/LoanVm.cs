﻿using DAL.Common;

namespace DAL.Models.Loan
{
    public class LoanVm
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public decimal InterestRate { get; set; }
        public LoanType LoanType { get; set; }
        public string LoanTypeInString { get; set; }
        public bool IsPaid { get; set; }
        public string Notes { get; set; }
        public string LoanProvidedDate { get; set; }
        public string ReypaymentDate { get; set; }
        public Guid TransactionId { get; set; }
        public string CreatedDate { get; set; }
    }

    public class LoanAmountVm
    {
        public decimal UnPaidLoanTakenAmount { get; set; }
        public decimal PaidLoanTakenAmount { get; set; }
        public decimal UnPaidLoanGivenAmount { get; set; }
        public decimal PaidLoanGivenAmount { get; set; }
        public decimal TotalLoanTakenAmount { get; set; }
        public decimal TotalLoanGivenAmount { get; set; }
    }

    public class LoanReportVm
    {
        public decimal UnPaidLoanTakenAmount { get; set; }
        public decimal PaidLoanTakenAmount { get; set; }
        public decimal UnPaidLoanGivenAmount { get; set; }
        public decimal PaidLoanGivenAmount { get; set; }
        public decimal TotalLoanTakenAmount { get; set; }
        public decimal TotalLoanGivenAmount { get; set; }
        public int UnPaidLoanTakenCount { get; set; }
        public int PaidLoanTakenCount { get; set; }
        public int UnPaidLoanGivenCount { get; set; }
        public int PaidLoanGivenCount { get; set; }
        public int TotalLoanTakenCount { get; set; }
        public int TotalLoanGivenCount { get; set; }
    }
}
