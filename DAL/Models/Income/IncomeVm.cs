﻿namespace DAL.Models.Income
{
    public class IncomeVm
    {
        public int Id { get; set; }
        public string Source { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public string Notes { get; set; }
        public string CreatedDate { get; set; }
        public Guid TransactionId { get; set; }
    }
}
