﻿namespace DAL.Models.Income
{
    public class CreateIncomeCommand
    {
        public string Source { get; set; }
        public decimal Amount { get; set; }
        public string Notes { get; set; }
    }
    public class UpdateIncomeCommand
    {
        public int Id { get; set; }
        public string Source { get; set; }
        public decimal Amount { get; set; }
        public string Notes { get; set; }
    }
}
