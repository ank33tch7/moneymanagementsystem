﻿namespace DAL.Models.StaticValues
{
    public class StaticValueTypeVm
    {
        public int Id { get; set; }
        public int SNO { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string AdditionalValue { get; set; }
    }
    public class StaticValueTypeForDropdownVm
    {
        public int SNO { get; set; }
        public string Name { get; set; }
    }
}
