﻿namespace DAL.Models.StaticValues
{
    public class CreateStaticValueTypeCommand
    {
        public int SNO { get; set; } 
        public string Name { get; set; }
        public string Description { get; set; }
        public string AdditionalValue { get; set; }
    }

    public class UpdateStaticValueTypeCommand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string AdditionalValue { get; set; }
    }
}
