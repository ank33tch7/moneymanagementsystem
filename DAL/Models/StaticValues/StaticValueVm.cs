﻿namespace DAL.Models.StaticValues
{
    public class StaticValueVm
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Data { get; set; }
        public string Description { get; set; }
        public string AdditionalValue { get; set; }
        public int TypeId { get; set; }
    }
}
