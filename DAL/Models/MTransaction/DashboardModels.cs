﻿namespace DAL.Models.MTransaction
{
    public class IncomeExpenseComparismVm
    {
        public decimal Amount { get; set; }
        public string TransactionType { get; set; }
    }
    public class TotalTransactionAmount
    {
        public decimal TotalIncome { get; set; }
        public decimal TotalExpense { get; set; }
        public decimal TotalLoanTaken { get; set; }
        public decimal TotalLoanGiven { get; set; }
        public decimal TotalInvestment { get; set; }
        public decimal TotalAmountSent { get; set; }
        public decimal BalanceAmount { get; set; }
        public decimal ActualAmount { get; set; }
    }

    public class ExpenseByCategoryVm
    {
        public string TotalExpense { get; set; }
        public string Category { get; set; }
    }


}
