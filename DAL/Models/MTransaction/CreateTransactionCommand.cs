﻿using DAL.Common;

namespace DAL.Models.MTransaction
{
    public class CreateTransactionCommand
    {
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public decimal InterestRate { get; set; }
        public LoanType LoanType { get; set; }
        public bool IsPaid { get; set; }
        public string Notes { get; set; }
        public DateTime LoanProvidedDate { get; set; }
        public DateTime ReypaymentDate { get; set; }
        public TransactionType TransactionType { get; set; }
    }
}
