﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class DbResponse
    {
        public int Code { get; private set; }
        public string Message { get; private set; }

        public static DbResponse CreateResponse(ResponeFlag flag)
        {
            switch (flag)
            {
                case ResponeFlag.UNAUTHORIZED:
                    return new DbResponse
                    {
                        Code = ResponseCodeConstants.UNAUTHORIZED,
                        Message = ResponseMessageConstants.UNAUTHORIZED
                    };
                case ResponeFlag.NOTFOUND:
                    return new DbResponse
                    {
                        Code = ResponseCodeConstants.NOTFOUND,
                        Message = ResponseMessageConstants.NOTFOUND
                    };
                case ResponeFlag.FAILED:
                    return new DbResponse
                    {
                        Code = ResponseCodeConstants.FAILED,
                        Message = ResponseMessageConstants.FAILED
                    };
                case ResponeFlag.EXCEPTION:
                    return new DbResponse
                    {
                        Code = ResponseCodeConstants.EXCEPTION,
                        Message = ResponseMessageConstants.EXCEPTION
                    };
                case ResponeFlag.SUCCESS:
                    return new DbResponse
                    {
                        Code = ResponseCodeConstants.SUCCESS,
                        Message = ResponseMessageConstants.SUCCESS
                    };
                default:
                    return new DbResponse
                    {
                        Code = ResponseCodeConstants.UNAUTHORIZED,
                        Message = ResponseMessageConstants.UNAUTHORIZED
                    };
            }
        }
    }

    public enum ResponeFlag
    {
        UNAUTHORIZED,
        NOTFOUND,
        FAILED,
        EXCEPTION,
        SUCCESS
    }
    public static class ResponseCodeConstants
    {
        public const int SUCCESS = 0;
        public const int FAILED = 1;
        public const int EXCEPTION = 999;
        public const int NOTFOUND = 404;
        public const int UNAUTHORIZED = 401;
    }
    public static class ResponseMessageConstants
    {
        public const string SUCCESS = "Success";
        public const string FAILED = "Failed";
        public const string EXCEPTION = "Something went wrong";
        public const string NOTFOUND = "Not Found";
        public const string UNAUTHORIZED = "Unauthorized Access";
    }
}
