﻿namespace DAL.Models
{
    public class DropDownModel
    {
        public string Id { get; set; }
        public string Label { get; set; }
    }
}
