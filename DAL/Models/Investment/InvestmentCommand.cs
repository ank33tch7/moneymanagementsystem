﻿namespace DAL.Models.Investment
{
    public class CreateInvestmentCommand
    {
        public string InvestmentType { get; set; }
        public string Notes { get; set; }
    }
    public class UpdateInvestmentCommand
    {
        public int Id { get; set; }
        public string InvestmentType { get; set; }
        public string Notes { get; set; }
    }
}
