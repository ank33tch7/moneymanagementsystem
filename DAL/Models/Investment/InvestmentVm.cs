﻿namespace DAL.Models.Investment
{
    public class InvestmentVm
    {
        public int Id { get; set; }
        public string InvestmentType { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public string Notes { get; set; }
        public Guid TransactionId { get; set; }
        public string CreatedDate { get; set; } 
    }
}
