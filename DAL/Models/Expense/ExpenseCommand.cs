﻿namespace DAL.Models.Expense
{
    public class CreateExpenseCommand
    {
        public decimal Amount { get; set; }
        public string Category { get; set; }
        public string Notes { get; set; }
    }
    public class UpdateExpenseCommand
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public string Category { get; set; }
        public string Notes { get; set; }
    }
}
