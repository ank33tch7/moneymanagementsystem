﻿namespace DAL.Models.Expense
{
    public class ExpenseVm
    {
        public int Id { get; set; }
        public string Category { get; set; }
        public string Name { get; set; }
        public string Notes { get; set; }
        public decimal Amount { get; set; }
        public string CreatedDate { get; set; }
        public Guid TransactionId { get; set; }
    }
    public class ExpenseAmountVm
    {
        public decimal ExpenseAmount { get; set; }
        public decimal SentHomeAmount { get; set; }
    }
}
