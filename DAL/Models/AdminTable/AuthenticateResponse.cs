﻿namespace DAL.Models.AdminTable
{
    public class AuthenticateResponse
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public TokenModel TokenModel { get; set; }
    }
}
