﻿namespace DAL.Models.AdminTable
{
    public record AuthenticateRequest
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
