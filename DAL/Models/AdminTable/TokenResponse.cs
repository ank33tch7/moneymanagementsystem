﻿namespace DAL.Models.AdminTable
{
    public class TokenResponse
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public string Token { get; set; }
    }
}
