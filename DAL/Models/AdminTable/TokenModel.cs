﻿using DAL.Services;

namespace DAL.Models.AdminTable
{
    public record TokenModel : ICurrentUserService
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}
