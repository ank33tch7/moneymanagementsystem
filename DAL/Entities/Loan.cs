﻿using DAL.Common;
using DAL.Entities.Auditable;

namespace DAL.Entities
{
    public class Loan : AuditableEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal InterestRate { get; set; }
        public LoanType LoanType { get; set; }
        public bool IsPaid { get; set; }
        public string Notes { get; set; }
        public DateTime LoanProvidedDate { get; set; }
        public DateTime ReypaymentDate { get; set; }
        public Guid TransactionId { get; set; }
        public MTransaction MTransaction { get; set; }
    }
}
