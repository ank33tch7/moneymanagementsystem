﻿using DAL.Entities.Auditable;

namespace DAL.Entities
{
    public class Expense : AuditableEntity
    {
        public int Id { get; set; }
        public string Category { get; set; }
        public string Notes { get; set; }
        public Guid TransactionId { get; set; }
        public MTransaction MTransaction { get; set; }
    }
}
