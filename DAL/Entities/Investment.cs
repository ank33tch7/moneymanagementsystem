﻿using DAL.Entities.Auditable;

namespace DAL.Entities
{
    public class Investment : AuditableEntity
    {
        public int Id { get; set; }
        public string InvestmentType { get; set; }
        public string Notes { get; set; }
        public Guid TransactionId { get; set; }
        public MTransaction MTransaction { get; set; }
    }
}
