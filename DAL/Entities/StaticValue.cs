﻿using DAL.Entities.Auditable;

namespace DAL.Entities
{
    public class StaticValue : AuditableEntity
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Data { get; set; }
        public string Description { get; set; }
        public string AdditionalValue { get; set; }
        public int TypeId { get; set; }
        //public StaticValueType StaticValueType { get; set; }
    }
}
