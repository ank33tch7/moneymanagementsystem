﻿using DAL.Entities.Auditable;

namespace DAL.Entities
{
    public class AdminTable : AuditableEntity
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public DateTime? Dob { get; set; }
        public DateTime? LastPasswordChangeDate { get; set; }
    }
}
