﻿using DAL.Common;
using DAL.Entities.Auditable;

namespace DAL.Entities
{
    public class MTransaction : AuditableEntity
    {
        public MTransaction()
        {
            Incomes = new List<Income>();
            Expenses = new List<Expense>();
            Investments = new List<Investment>();
            Loans = new List<Loan>();
        }
        public Guid Id { get; set; }
        public decimal Amount { get; set; }
        public TransactionType TransactionType { get; set; }
        public ICollection<Income> Incomes { get; set; }
        public ICollection<Expense> Expenses { get; set; }
        public ICollection<Investment> Investments { get; set; }
        public ICollection<Loan> Loans { get; set; }

        public void AddIncome(Income income)
        {
            Incomes.Add(income);
        }

        public void AddExpense(Expense expense)
        {
            Expenses.Add(expense);
        }
        public void AddInvestment(Investment investment)
        {
            Investments.Add(investment);
        }
        public void AddLoan(Loan loan)
        {
            Loans.Add(loan);
        }

    }
}
