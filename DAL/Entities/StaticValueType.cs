﻿using DAL.Entities.Auditable;

namespace DAL.Entities
{
    public class StaticValueType : AuditableEntity
    {
        public StaticValueType()
        {
            //StaticValues = new List<StaticValue>();
        }
        public int Id { get; set; }
        public int SNO { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string AdditionalValue { get; set; }
        //public ICollection<StaticValue> StaticValues { get; set; }

    }
}
