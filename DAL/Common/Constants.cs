﻿namespace DAL.Common
{
    public static class JwtTokenContants
    {
        public const string UserId = "userId";
        public const string UserName = "username";
        public const string Phone = "phone";
        public const string Email = "email";
        public const string FullName = "fullName";
        public const string Address = "address";
    }
}
