﻿using System.ComponentModel.DataAnnotations;

namespace DAL.Common
{
    public enum LoanType
    {
        [Display(Name = "Loan Taken")]
        Taken,
        [Display(Name = "Loan Given")]
        Given
    }
    public enum TransactionType
    {
        [Display(Name = "Income")]
        Income,
        [Display(Name = "Expense")]
        Expense,
        [Display(Name = "Investment")]
        Investment,
        [Display(Name = "Loan")]
        Loan
    }
    
}
