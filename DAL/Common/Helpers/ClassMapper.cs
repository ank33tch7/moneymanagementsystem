﻿namespace DAL.Common.Helpers
{
    public class ClassMapper<TSource, TDestination> 
    { 
        public TDestination Map(TSource source)
        {
            TDestination destination = Activator.CreateInstance<TDestination>();

            var sourceProperties = typeof(TSource).GetProperties();
            var destinationProperties = typeof(TDestination).GetProperties();

            foreach ( var property in sourceProperties )
            {
                var destinationProperty = destinationProperties.FirstOrDefault(x => x.Name == property.Name);
                if( destinationProperty != null && destinationProperty.PropertyType == property.PropertyType)
                {
                    var value = property.GetValue(source);
                    destinationProperty.SetValue(destination, value);
                }
            }
            return destination;
        }
    }
}
