﻿namespace DAL.Common
{
    public enum TransactionCategory
    {
        Income=1,
        Expense,
        Investment,
        LoanTaken,
        LoanGiven
    }
}
