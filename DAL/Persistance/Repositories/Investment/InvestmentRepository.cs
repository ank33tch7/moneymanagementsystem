﻿using DAL.Models.Income;
using DAL.Models.Investment;
using DAL.Persistance.DataContext;
using DAL.Persistance.SqlConnect;
using Microsoft.EntityFrameworkCore;

namespace DAL.Persistance.Repositories.Investments
{
    public class InvestmentRepository : IInvestmentRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly ISQLHelper _sqlHelper;
        public InvestmentRepository(ApplicationDbContext context, ISQLHelper sQLHelper)
        {
            _context = context;
            _sqlHelper = sQLHelper;
        }

        public async Task<List<InvestmentVm>> GetInvestments()
        {
            //var investmentList = await _context.Investments
            //                .Select(x => new InvestmentVm
            //                {
            //                    Id = x.Id,
            //                    Amount = x.MTransaction.Amount,
            //                    InvestmentType = x.InvestmentType,
            //                    Notes = x.Notes,
            //                    CreatedDate = x.CreatedDate.ToString("yyyy-MM-dd"),
            //                    TransactionId = x.TransactionId
            //                }).ToListAsync();
            //return investmentList;
            string investmentSql = "SELECT FORMAT(i.CreatedDate, 'yyyy-MM-dd') CreatedDate,t.Amount, sv.Data InvestmentType, i.Notes , t.Id TransactionId , sv.Value Name FROM Investments i JOIN MTransactions t on i.TransactionId = t.Id JOIN StaticValues sv on i.InvestmentType = sv.Value";
            var investmentList = await _sqlHelper.LoadSQLDataListAsync<InvestmentVm>(investmentSql);
            return investmentList;
        }

        public async Task<decimal> GetTotalInvestmentAmount()
        {
            var totalInvestmentSql = "SELECT ISNULL(SUM(t.Amount),0) TotalInvestment FROM Investments i WITH(NOLOCK) JOIN MTransactions t WITH(NOLOCK) ON i.TransactionId = t.Id";
            var totalInvestment = await _sqlHelper.LoadSQLDataSingleAsync<decimal>(totalInvestmentSql);
            return totalInvestment;
        }
    }
}
