﻿using DAL.Models.Investment;

namespace DAL.Persistance.Repositories.Investments
{
    public interface IInvestmentRepository
    {
        Task<List<InvestmentVm>> GetInvestments();
        Task<decimal> GetTotalInvestmentAmount();
    }
}
