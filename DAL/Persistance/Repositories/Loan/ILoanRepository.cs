﻿using DAL.Common;
using DAL.Models;
using DAL.Models.Loan;

namespace DAL.Persistance.Repositories.Loans
{
    public interface ILoanRepository
    {
        Task<List<LoanVm>> GetLoans();
        Task<List<LoanVm>> GetLoanByType(LoanType type);
        Task<DbResponse> SetAsLoanPaid(Guid id);
        Task<LoanAmountVm> GetLoanAmount();
        Task<LoanReportVm> LoanReportDetail();

        //Task<LoanAmountVm> GetAmountDetails();
    }
}
