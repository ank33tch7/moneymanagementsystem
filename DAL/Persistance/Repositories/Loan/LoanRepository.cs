﻿using DAL.Common;
using DAL.Models;
using DAL.Models.Loan;
using DAL.Persistance.DataContext;
using DAL.Persistance.SqlConnect;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace DAL.Persistance.Repositories.Loans
{
    public class LoanRepository : ILoanRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly ISQLHelper _sqlHelper;
        public LoanRepository(ApplicationDbContext context, ISQLHelper sQLHelper)
        {
            _context = context;
            _sqlHelper = sQLHelper;
        }

        public async Task<List<LoanVm>> GetLoans()
        {
            var loanList = await _context.Loans
                            .Select(x => new LoanVm
                            {
                                Id = x.Id,
                                Amount = x.MTransaction.Amount,
                                Notes = x.Notes,
                                InterestRate = x.InterestRate,
                                IsPaid = x.IsPaid,
                                LoanProvidedDate = x.LoanProvidedDate.ToString("yyyy-MM-dd"),
                                LoanTypeInString = x.LoanType.GetEnumDisplayName(),
                                LoanType = x.LoanType,
                                Name = x.Name,

                                ReypaymentDate = x.ReypaymentDate.ToString("yyyy-MM-dd"),
                                CreatedDate = x.CreatedDate.ToString("yyyy-MM-dd"),
                                TransactionId = x.TransactionId
                            }).ToListAsync();
            return loanList;
        }
        public async Task<List<LoanVm>> GetLoanByType(LoanType type)
        {
            var loanList = await _context.Loans
                            .AsNoTracking()
                            .Where(x => x.LoanType == type)
                            .Select(x => new LoanVm
                            {
                                Id = x.Id,
                                Amount = x.MTransaction.Amount,
                                Notes = x.Notes,
                                InterestRate = x.InterestRate,
                                IsPaid = x.IsPaid,
                                LoanProvidedDate = x.LoanProvidedDate.ToString("yyyy-MM-dd"),
                                LoanTypeInString = x.LoanType.GetEnumDisplayName(),
                                LoanType = x.LoanType,
                                Name = x.Name,

                                ReypaymentDate = x.ReypaymentDate.ToString("yyyy-MM-dd"),
                                CreatedDate = x.CreatedDate.ToString("yyyy-MM-dd"),
                                TransactionId = x.TransactionId
                            }).ToListAsync();
            return loanList;
        }
        public async Task<LoanAmountVm> GetLoanAmount()
        {
            var sp = "spa_loan_amount_details";
            var result = await _sqlHelper.LoadSPDataSingleWithoutParamAsync<LoanAmountVm>(sp);
            return result;
        }

        public async Task<LoanReportVm> LoanReportDetail()
        {
            var sp = "spa_loan_reportDetail";
            var result = await _sqlHelper.LoadSPDataSingleWithoutParamAsync<LoanReportVm>(sp);
            return result;
            //var (uLoanTaken, pLoanTaken, uLoanGiven,
            //    pLoanGiven, totalLoanTaken, totalLoanGiven,
            //    uLoanTakenCount, pLoanTakenCount, uLoanGivenCount,pLoanGivenCount,
            //    tLoanTakenCount, tLoanGivenCount) = (await _context.Loans
            //        .GroupBy(x => 1)
            //        .AsNoTracking()
            //        .Select(y => new
            //        {
            //            uLoanTaken = y.Where(x => x.IsPaid == false && x.LoanType == LoanType.Taken).Sum(x => x.MTransaction.Amount),
            //            pLoanTaken = y.Where(x => x.IsPaid == true && x.LoanType == LoanType.Taken).Sum(x => x.MTransaction.Amount),
            //            uLoanGiven = y.Where(x => x.IsPaid == false && x.LoanType == LoanType.Given).Sum(x => x.MTransaction.Amount),
            //            pLoanGiven = y.Where(x => x.IsPaid == true && x.LoanType == LoanType.Given).Sum(x => x.MTransaction.Amount),
            //            totalLoanTaken = y.Where(x => x.LoanType == LoanType.Taken).Sum(x => x.MTransaction.Amount),
            //            totalLoanGiven = y.Where(x => x.LoanType == LoanType.Given).Sum(x => x.MTransaction.Amount),
            //            uLoanTakenCount = y.Count(x => x.IsPaid == false && x.LoanType == LoanType.Taken),
            //            pLoanTakenCount = y.Count(x => x.IsPaid == true && x.LoanType == LoanType.Taken),
            //            uLoanGivenCount = y.Count(x => x.IsPaid == false && x.LoanType == LoanType.Given),
            //            pLoanGivenCount = y.Count(x => x.IsPaid == true && x.LoanType == LoanType.Given),
            //            tLoanTakenCount = y.Count(x => x.LoanType == LoanType.Taken),
            //            tLoanGivenCount = y.Count(x => x.LoanType == LoanType.Given)
            //        }).ToListAsync())
            //        .Select(z => (
            //            z.uLoanTaken,
            //            z.pLoanTaken,
            //            z.uLoanGiven,
            //            z.pLoanGiven,
            //            z.totalLoanTaken,
            //            z.totalLoanGiven,
            //            z.uLoanTakenCount,
            //            z.pLoanTakenCount,
            //            z.uLoanGivenCount,
            //            z.pLoanGivenCount,
            //            z.tLoanTakenCount,
            //            z.tLoanGivenCount
            //        )).FirstOrDefault();

            //return new LoanReportVm
            //{
            //    UnPaidLoanTakenAmount = uLoanTaken,
            //    PaidLoanTakenAmount = pLoanTaken,
            //    UnPaidLoanGivenAmount = uLoanGiven,
            //    PaidLoanGivenAmount = pLoanGiven,

            //    TotalLoanTakenAmount = totalLoanTaken,
            //    TotalLoanGivenAmount = totalLoanGiven,

            //    UnPaidLoanTakenCount = uLoanTakenCount,
            //    PaidLoanTakenCount = pLoanTakenCount,

            //    UnPaidLoanGivenCount = uLoanGivenCount,
            //    PaidLoanGivenCount = pLoanGivenCount,

            //    TotalLoanTakenCount = tLoanTakenCount,
            //    TotalLoanGivenCount = pLoanGivenCount,
            //};
        }

        public async Task<DbResponse> SetAsLoanPaid(Guid id)
        {
            try
            {
                var loanDetail = await _context.Loans.SingleOrDefaultAsync(x => x.TransactionId == id);
                if (loanDetail is null)
                    return DbResponse.CreateResponse(ResponeFlag.NOTFOUND);
                loanDetail.IsPaid = loanDetail.IsPaid is false ? true : false;
                var result = await _context.SaveChangesAsync();
                if (result > 0)
                    return DbResponse.CreateResponse(ResponeFlag.SUCCESS);
                return DbResponse.CreateResponse(ResponeFlag.FAILED);
            }
            catch
            {
                return DbResponse.CreateResponse(ResponeFlag.EXCEPTION);
            }

        }




        //public async Task<LoanAmountVm> GetAmountDetails()
        //{
        //    var (uLoanTaken, pLoanTaken, uLoanGiven, pLoanGiven, totalLoanTaken, totalLoanGiven) = (await _context.Loans
        //            .GroupBy(x => 1)
        //            .AsNoTracking()
        //            .Select(y => new
        //            {
        //                uLoanTaken = y.Where(x => x.IsPaid == false && x.LoanType == LoanType.Taken).Sum(x => x.MTransaction.Amount),
        //                pLoanTaken = y.Where(x => x.IsPaid == true && x.LoanType == LoanType.Taken).Sum(x => x.MTransaction.Amount),
        //                uLoanGiven = y.Where(x => x.IsPaid == false && x.LoanType == LoanType.Given).Sum(x => x.MTransaction.Amount),
        //                pLoanGiven = y.Where(x => x.IsPaid == true && x.LoanType == LoanType.Given).Sum(x => x.MTransaction.Amount),
        //                totalLoanTaken = y.Where(x =>  x.LoanType == LoanType.Taken).Sum(x => x.MTransaction.Amount),
        //                totalLoanGiven = y.Where(x =>  x.LoanType == LoanType.Given).Sum(x => x.MTransaction.Amount)
        //            }).ToListAsync()).Select(z => (
        //                z.uLoanTaken,
        //                z.pLoanTaken,
        //                z.uLoanGiven,
        //                z.pLoanGiven,
        //                z.totalLoanTaken,
        //                z.totalLoanGiven
        //            )).FirstOrDefault();
        //    return new LoanAmountVm
        //    {
        //        UnPaidLoanTakenAmount = uLoanTaken,
        //        PaidLoanTakenAmount = pLoanTaken,
        //        UnPaidLoanGivenAmount = uLoanGiven,
        //        PaidLoanGivenAmount = pLoanGiven,
        //        TotalLoanTakenAmount = totalLoanTaken,
        //        TotalLoanGivenAmount = totalLoanGiven,
        //    };


        //}

        //public async Task<double> GetTotalLoanUnpaidTakenAmount()
        //{
        //    string totalLoanTakenSql = "SELECT ISNULL(SUM(t.Amount),0) TotalLoanTaken FROM Loans l WITH(NOLOCK) JOIN MTransactions t WITH(NOLOCK) ON l.TransactionId = t.Id WHERE l.LoanType = 0 AND ISPAID = 0";
        //    var totalLoanTaken = await _sqlHelper.LoadSQLDataSingleAsync<double>(totalLoanTakenSql);
        //    return totalLoanTaken;
        //}
        //public async Task<double> GetTotalLoanPaidTakenAmount()
        //{
        //    string totalLoanTakenSql = "SELECT ISNULL(SUM(t.Amount),0) TotalLoanTaken FROM Loans l WITH(NOLOCK) JOIN MTransactions t WITH(NOLOCK) ON l.TransactionId = t.Id WHERE l.LoanType = 0 AND ISPAID = 1";
        //    var totalLoanTaken = await _sqlHelper.LoadSQLDataSingleAsync<double>(totalLoanTakenSql);
        //    return totalLoanTaken;
        //}
        //public async Task<double> GetTotalLoanUnpaidGivenAmount()
        //{
        //    string totalLoanGivenSql = "SELECT ISNULL(SUM(t.Amount),0) TotalLoanGiven FROM Loans l WITH(NOLOCK) JOIN MTransactions t WITH(NOLOCK) ON l.TransactionId = t.Id WHERE l.LoanType = 1 AND ISPAID = 0";
        //    var totalLoanGiven = await _sqlHelper.LoadSQLDataSingleAsync<double>(totalLoanGivenSql);
        //    return totalLoanGiven;
        //}
        //public async Task<double> GetTotalLoanPaidGivenAmount()
        //{
        //    string totalLoanGivenSql = "SELECT ISNULL(SUM(t.Amount),0) TotalLoanGiven FROM Loans l WITH(NOLOCK) JOIN MTransactions t WITH(NOLOCK) ON l.TransactionId = t.Id WHERE l.LoanType = 1 AND ISPAID = 1";
        //    var totalLoanGiven = await _sqlHelper.LoadSQLDataSingleAsync<double>(totalLoanGivenSql);
        //    return totalLoanGiven;
        //}
    }
}
