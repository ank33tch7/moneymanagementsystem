﻿using DAL.Models.MTransaction;
using DAL.Models;
using DAL.Common;

namespace DAL.Persistance.Repositories.MTransactions
{
    public interface IMTransactionRepository
    {
        Task<DbResponse> CreateTransaction(CreateTransactionCommand command);
        Task<DbResponse> UpdateTransaction(UpdateTansactionCommand command);
        Task<DbResponse> DeleteTransaction(TransactionType transactionType, Guid id);

        Task<List<IncomeExpenseComparismVm>> IncomeExpenseComparism();
       
    }
}
