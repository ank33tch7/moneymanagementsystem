﻿using DAL.Common;
using DAL.Entities;
using DAL.Models;
using DAL.Models.MTransaction;
using DAL.Persistance.DataContext;
using DAL.Persistance.SqlConnect;
using Microsoft.EntityFrameworkCore;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace DAL.Persistance.Repositories.MTransactions
{
    public class MTransactionRepository : IMTransactionRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly ISQLHelper _sqlHelper;
        public MTransactionRepository(ApplicationDbContext context, ISQLHelper sQLHelper)
        {
            _context = context;
            _sqlHelper = sQLHelper;
        }

        public async Task<DbResponse> CreateTransaction(CreateTransactionCommand command)
        {
            try
            {
                var mtransaction = new MTransaction
                {
                    Amount = command.Amount,
                    TransactionType = command.TransactionType,
                };
               
                switch (command.TransactionType)
                {
                    case TransactionType.Income:
                        var income = new Income
                        {
                            Source = command.Name,
                            Notes = command.Notes,
                        };
                        mtransaction.AddIncome(income);
                        break;

                    case TransactionType.Expense:
                        var expense = new Expense
                        {
                            Category = command.Name,
                            Notes = command.Notes,
                        };
                        mtransaction.AddExpense(expense);
                        break;

                    case TransactionType.Investment:
                        var investment = new Investment
                        {
                            InvestmentType = command.Name,
                            Notes = command.Notes,
                        };
                        mtransaction.AddInvestment(investment);
                        break;
                        
                    case TransactionType.Loan:
                        var loan = new Loan
                        {
                            Name = command.Name,
                            InterestRate = command.InterestRate,
                            LoanType    = command.LoanType,
                            Notes = command.Notes,
                            LoanProvidedDate = command.LoanProvidedDate,
                            ReypaymentDate = command.ReypaymentDate
                        };
                        mtransaction.AddLoan(loan);
                        break;
                }

                _context.MTransactions.Add(mtransaction);
                var result = await _context.SaveChangesAsync();
                if (result > 0)
                    return DbResponse.CreateResponse(ResponeFlag.SUCCESS);
                return DbResponse.CreateResponse(ResponeFlag.FAILED);
            }
            catch
            {
                return DbResponse.CreateResponse(ResponeFlag.EXCEPTION);
            }
        }
        public async Task<DbResponse> UpdateTransaction(UpdateTansactionCommand command)
        {
            try
            {
                var transactionDetail = await _context.MTransactions.FindAsync(command.TransactionId);
                if(transactionDetail is null)
                    return DbResponse.CreateResponse(ResponeFlag.NOTFOUND);

                switch (command.TransactionType)
                {
                    case TransactionType.Income:
                        var incomeDetail = await _context.Incomes.SingleOrDefaultAsync(x => x.TransactionId == command.TransactionId);
                        incomeDetail.Notes = command.Notes;
                        incomeDetail.Source = command.Name;
                        break;

                    case TransactionType.Expense:
                        var expenseDetail = await _context.Expenses.SingleOrDefaultAsync(x => x.TransactionId == command.TransactionId);
                        expenseDetail.Notes = command.Notes;
                        expenseDetail.Category = command.Name;
                        break;

                    case TransactionType.Investment:
                        var investmentDetail = await _context.Investments.SingleOrDefaultAsync(x => x.TransactionId == command.TransactionId);
                        investmentDetail.InvestmentType = command.Name;
                        investmentDetail.Notes = command.Notes;
                        break;

                    case TransactionType.Loan:
                        var loanDetail = await _context.Loans.SingleOrDefaultAsync(x => x.TransactionId == command.TransactionId);
                        loanDetail.Notes = command.Notes;
                        loanDetail.Name = command.Name;
                        loanDetail.InterestRate = command.InterestRate;
                        loanDetail.LoanType = command.LoanType;
                        loanDetail.LoanProvidedDate = command.LoanProvidedDate;
                        loanDetail.ReypaymentDate = command.ReypaymentDate;
                        break;
                }

                transactionDetail.Amount = command.Amount;

                var result = await _context.SaveChangesAsync();
                if (result > 0)
                    return DbResponse.CreateResponse(ResponeFlag.SUCCESS);
                return DbResponse.CreateResponse(ResponeFlag.FAILED);
            }
            catch
            {
                return DbResponse.CreateResponse(ResponeFlag.EXCEPTION);
            }
        }
        public async Task<DbResponse> DeleteTransaction(TransactionType transactionType, Guid id)
        {
            try
            {
                var transaction = await _context.MTransactions.FindAsync(id);

                switch (transactionType)
                {
                    case TransactionType.Income:
                        var income =  _context.Incomes.FirstOrDefault(x => x.TransactionId == id);
                        _context.Incomes.Remove(income);
                        break;
                    case TransactionType.Expense:
                        var expense = _context.Expenses.FirstOrDefault(x => x.TransactionId == id);
                        _context.Expenses.Remove(expense);
                        break;

                    case TransactionType.Investment:
                        var investment = _context.Investments.FirstOrDefault(x => x.TransactionId == id);
                        _context.Investments.Remove(investment);
                        break;
                    case TransactionType.Loan:
                        var loan = _context.Loans.FirstOrDefault(x => x.TransactionId == id);
                        _context.Loans.Remove(loan);
                        break;
                }

                _context.MTransactions.Remove(transaction);
                var result = await _context.SaveChangesAsync();
                if (result > 0)
                    return DbResponse.CreateResponse(ResponeFlag.SUCCESS);
                return DbResponse.CreateResponse(ResponeFlag.FAILED);
            }
            catch
            {
                return DbResponse.CreateResponse(ResponeFlag.EXCEPTION);
            }
        }



        // for dashboard:

        // income expense comparism
        public async Task<List<IncomeExpenseComparismVm>> IncomeExpenseComparism()
        {
            var comparism = await _context.MTransactions
                                            .Where(x => x.CreatedDate >= DateTime.UtcNow.AddDays(-30))
                                            .GroupBy(x => x.TransactionType)
                                            
                                            .Select(x => new IncomeExpenseComparismVm
                                            {
                                                Amount = x.Sum(x => x.Amount),
                                                TransactionType = x.FirstOrDefault().TransactionType.GetEnumDisplayName(),
                                            }).ToListAsync();

            return comparism;
        }
    }
}
