﻿using DAL.Models.Expense;
using DAL.Models.MTransaction;

namespace DAL.Persistance.Repositories.Expenses
{
    public interface IExpenseRepository
    {
        Task<List<ExpenseVm>> GetExpenses();
        //Task<double> GetTotalExpenseAmount();
        //Task<double> GetTotalAmountSendToHome();
        Task<List<ExpenseByCategoryVm>> GetExpenseByCategories();
        //Task<ExpenseAmountVm> GetExpenseAmountDetails();

        Task<ExpenseAmountVm> GetExpenseAmount();
    }
}
