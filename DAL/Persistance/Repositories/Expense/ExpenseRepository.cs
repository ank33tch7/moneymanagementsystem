﻿using DAL.Models.Expense;
using DAL.Models.MTransaction;
using DAL.Persistance.DataContext;
using DAL.Persistance.SqlConnect;
using Microsoft.EntityFrameworkCore;

namespace DAL.Persistance.Repositories.Expenses
{
    public class ExpenseRepository : IExpenseRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly ISQLHelper _sqlHelper;
        public ExpenseRepository(ApplicationDbContext context, ISQLHelper sQLHelper)
        {
            _context = context;
            _sqlHelper = sQLHelper;
        }

        public async Task<List<ExpenseVm>> GetExpenses()
        {
            //var expenseList = await _context.Expenses
            //                .Select(x => new ExpenseVm
            //                {
            //                    Id = x.Id,
            //                    Amount = x.MTransaction.Amount,
            //                    Notes = x.Notes,
            //                    Category = x.Category,
            //                    CreatedDate = x.CreatedDate.ToString("yyyy-MM-dd"),
            //                    TransactionId = x.TransactionId
            //                }).ToListAsync();

            string expenseSql = "SELECT FORMAT(e.CreatedDate, 'yyyy-MM-dd') CreatedDate,t.Amount, sv.Data Category, e.Notes, t.Id TransactionId, sv.Value Name FROM Expenses e JOIN MTransactions t on e.TransactionId = t.Id JOIN StaticValues sv on e.Category = sv.Value";
            var expenseList = await _sqlHelper.LoadSQLDataListAsync<ExpenseVm>(expenseSql);
            return expenseList;
        }
        public async Task<ExpenseAmountVm> GetExpenseAmount()
        {
            var sp = "spa_expense_amount";
            var result = await _sqlHelper.LoadSPDataSingleWithoutParamAsync<ExpenseAmountVm>(sp);
            return result;
        }
        public async Task<List<ExpenseByCategoryVm>> GetExpenseByCategories()
        {
            string sql = "SELECT ISNULL(SUM(t.Amount), 0) AS TotalExpense, sv.Data AS Category FROM StaticValues sv WITH(NOLOCK) LEFT OUTER JOIN Expenses e WITH(NOLOCK) ON sv.Value = e.Category  LEFT OUTER JOIN MTransactions t WITH(NOLOCK) ON e.TransactionId = t.Id WHERE  sv.Value NOT IN ('99') AND sv.Description = 'Expense Category' GROUP BY sv.Data, sv.Value";
            var result = await _sqlHelper.LoadSQLDataListAsync<ExpenseByCategoryVm>(sql);
            return result;
        }



        public async Task<ExpenseAmountVm> GetExpenseAmountDetails()
        {
            var (expenseAmt, sentHomeAmt) = (await _context.Expenses
                                            .GroupBy(x => 1)
                                            .AsNoTracking()
                                            .Select(y => new
                                            {
                                                expenseAmt = y.Where(a => a.Category != "99").Sum(a => a.MTransaction.Amount),
                                                sentHomeAmt = y.Where(a => a.Category == "99").Sum(a => a.MTransaction.Amount)
                                            }).ToListAsync())
                                            .Select(z =>
                                            (
                                                z.expenseAmt,
                                                z.sentHomeAmt
                                            )).FirstOrDefault();

            return new ExpenseAmountVm
            {
                ExpenseAmount = expenseAmt,
                SentHomeAmount = sentHomeAmt
            };
        }

        //public async Task<double> GetTotalExpenseAmount()
        //{
        //    var totalExpenseSql = "SELECT ISNULL(SUM(t.Amount),0) TotalExpense FROM Expenses e WITH(NOLOCK) JOIN MTransactions t WITH(NOLOCK) ON e.TransactionId = t.Id  AND e.Category NOT IN ('99')";
        //    var totalExpense = await _sqlHelper.LoadSQLDataSingleAsync<double>(totalExpenseSql);
        //    return totalExpense;
        //}
        //public async Task<double> GetTotalAmountSendToHome()
        //{
        //    var totalExpenseSql = "SELECT ISNULL(SUM(t.Amount),0) TotalExpense FROM Expenses e WITH(NOLOCK) JOIN MTransactions t WITH(NOLOCK) ON e.TransactionId = t.Id  AND e.Category IN ('99')";
        //    var totalExpense = await _sqlHelper.LoadSQLDataSingleAsync<double>(totalExpenseSql);
        //    return totalExpense;
        //}
    }
}
