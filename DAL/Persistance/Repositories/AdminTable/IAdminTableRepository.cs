﻿using DAL.Models.AdminTable;
using DAL.Models;

namespace DAL.Persistance.Repositories.AdminTables
{
    public interface IAdminTableRepository
    {
        Task<DbResponse> CreateUser(CreateUserCommand command);
        Task<AuthenticateResponse> AuthenticateUser(AuthenticateRequest request);
    }
}
