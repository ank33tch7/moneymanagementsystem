﻿using DAL.Common.Helpers;
using DAL.Entities;
using DAL.Models;
using DAL.Models.AdminTable;
using DAL.Persistance.DataContext;
using Microsoft.EntityFrameworkCore;

namespace DAL.Persistance.Repositories.AdminTables
{
    public class AdminTableRepository : IAdminTableRepository
    {
        private readonly ApplicationDbContext _context;
        public AdminTableRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<DbResponse> CreateUser(CreateUserCommand command)
        {
            try
            {
                command.Password = StaticData.EncryptPassword(command.Password);
                var mapper = new ClassMapper<CreateUserCommand, AdminTable>();
                var user = mapper.Map(command);
                _context.AdminTable.Add(user);
                var result = await _context.SaveChangesAsync();
                if(result>0)
                    return DbResponse.CreateResponse(ResponeFlag.SUCCESS);
                return DbResponse.CreateResponse(ResponeFlag.FAILED);
            }
            catch
            {
                return DbResponse.CreateResponse(ResponeFlag.EXCEPTION);
            }
        }

        public async Task<AuthenticateResponse> AuthenticateUser(AuthenticateRequest request)
        {
            var user = await _context.AdminTable.FirstOrDefaultAsync(x => x.UserName == request.UserName);
            if (user is null)
                return new AuthenticateResponse
                {
                    Code = 400,
                    Message = "Unauthorized user."
                };

            var isValidPassword = StaticData.ComparePassword(request.Password,user.Password);

            if (!isValidPassword)
                return new AuthenticateResponse
                {
                    Code = 422,
                    Message = "Password did not matched."
                };

            if(!user.IsActive)
                return new AuthenticateResponse
                {
                    Code = 400,
                    Message = "User is not active. Please contact to administrator."
                };

            return new AuthenticateResponse
            {
                Code = 200,
                Message = "Authorized User",
                TokenModel = new TokenModel
                {
                    UserId = user.Id.ToString(),
                    FullName = user.FullName,
                    Email = user.Email,
                    UserName = user.UserName,
                    Phone = user.Phone
                }
            };
        }
    }
}
