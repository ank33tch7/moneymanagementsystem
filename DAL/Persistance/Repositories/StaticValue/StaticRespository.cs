﻿using DAL.Common;
using DAL.Common.Helpers;
using DAL.Entities;
using DAL.Models;
using DAL.Models.StaticValues;
using DAL.Persistance.DataContext;
using DAL.Persistance.SqlConnect;
using Microsoft.EntityFrameworkCore;

namespace DAL.Persistance.Repositories.StaticValues
{
    public class StaticRespository : IStaticRespository
    {
        private readonly ApplicationDbContext _context;
        private readonly ISQLHelper _sqlHelper;
        public StaticRespository(ApplicationDbContext context, ISQLHelper sQLHelper)
        {
            _context = context;
            _sqlHelper = sQLHelper;
        }

        // static value types
        public async Task<List<StaticValueTypeVm>> GetStaticValueTypes()
        {
            var staticValueTypes = await _context.StaticValueTypes
                                                .AsNoTracking()
                                                .Select(x => new StaticValueTypeVm
                                                {
                                                    Id = x.Id,
                                                    Name = x.Name,
                                                    AdditionalValue = x.AdditionalValue,
                                                    Description = x.Description,
                                                    SNO = x.SNO,
                                                }).ToListAsync();
            return staticValueTypes;
        }

        public async Task<List<StaticValueTypeForDropdownVm>> GetStaticValueTypeForDropdown()
        {
            var staticValueTypes = await _context.StaticValueTypes
                                                .AsNoTracking()
                                                .Select(x => new StaticValueTypeForDropdownVm
                                                {
                                                    Name = x.Name,
                                                    SNO = x.SNO
                                                }).ToListAsync();
            return staticValueTypes;
        }
        public async Task<DbResponse> CreateStaticValueType(CreateStaticValueTypeCommand command)
        {
            try
            {
                var mapper = new ClassMapper<CreateStaticValueTypeCommand, StaticValueType>();
                var staticValueType = mapper.Map(command);

                _context.StaticValueTypes.Add(staticValueType);

                var result = await _context.SaveChangesAsync();
                if (result > 0)
                    return DbResponse.CreateResponse(ResponeFlag.SUCCESS);
                return DbResponse.CreateResponse(ResponeFlag.FAILED);
            }
            catch
            {
                return DbResponse.CreateResponse(ResponeFlag.EXCEPTION);
            }
        }
        public async Task<DbResponse> UpdateStaticValueType(UpdateStaticValueTypeCommand command)
        {
            try
            {
                var staticValueType = await _context.StaticValueTypes.FindAsync(command.Id);
                if (staticValueType is null)
                    return DbResponse.CreateResponse(ResponeFlag.NOTFOUND);

                staticValueType.Name = command.Name;
                staticValueType.Description = command.Description;
                staticValueType.AdditionalValue = command.AdditionalValue;

                var result = await _context.SaveChangesAsync();
                if (result > 0)
                    return DbResponse.CreateResponse(ResponeFlag.SUCCESS);
                return DbResponse.CreateResponse(ResponeFlag.FAILED);
            }
            catch
            {
                return DbResponse.CreateResponse(ResponeFlag.EXCEPTION);
            }
        }

        public async Task<DbResponse> DeleteStaticValueType(int id)
        {
            try
            {
                var staticValueType = await _context.StaticValueTypes.FindAsync(id);
                if (staticValueType is null)
                    return DbResponse.CreateResponse(ResponeFlag.NOTFOUND);

                var staticValue = await _context.StaticValues.FirstOrDefaultAsync(x => x.TypeId == staticValueType.SNO);

                if (staticValue is not null)
                    _context.StaticValues.Remove(staticValue);

                _context.StaticValueTypes.Remove(staticValueType);

                var result = await _context.SaveChangesAsync();
                if (result > 0)
                    return DbResponse.CreateResponse(ResponeFlag.SUCCESS);
                return DbResponse.CreateResponse(ResponeFlag.FAILED);
            }
            catch
            {
                return DbResponse.CreateResponse(ResponeFlag.EXCEPTION);
            }
        }


        // static values
        public async Task<List<StaticValueVm>> GetStaticValues()
        {
            var staticValues = await _context.StaticValues
                                                .AsNoTracking()
                                                .Select(x => new StaticValueVm
                                                {
                                                    Id = x.Id,
                                                    Value = x.Value,
                                                    AdditionalValue = x.AdditionalValue,
                                                    Description = x.Description,
                                                    Data = x.Data,
                                                    TypeId = x.TypeId
                                                }).ToListAsync();
            return staticValues;
        }
        public async Task<List<StaticValueVm>> GetStaticValuesByTypeId(int typeId)
        {
            var staticValues = await _context.StaticValues
                                                .AsNoTracking()
                                                .Where(x => x.TypeId == typeId)
                                                .Select(x => new StaticValueVm
                                                {
                                                    Id = x.Id,
                                                    Value = x.Value,
                                                    AdditionalValue = x.AdditionalValue,
                                                    Description = x.Description,
                                                    Data = x.Data,
                                                    TypeId = x.TypeId
                                                }).ToListAsync();
            return staticValues;
        }
        public async Task<DbResponse> CreateStaticValue(CreateStaticValueCommand command)
        {
            try
            {
                var mapper = new ClassMapper<CreateStaticValueCommand, StaticValue>();
                var staticValue = mapper.Map(command);

                _context.StaticValues.Add(staticValue);

                var result = await _context.SaveChangesAsync();
                if (result > 0)
                    return DbResponse.CreateResponse(ResponeFlag.SUCCESS);
                return DbResponse.CreateResponse(ResponeFlag.FAILED);
            }
            catch
            {
                return DbResponse.CreateResponse(ResponeFlag.EXCEPTION);
            }
        }
        public async Task<DbResponse> UpdateStaticValue(UpdateStaticValueCommand command)
        {
            try
            {
                var staticValue = await _context.StaticValues.FindAsync(command.Id);
                if (staticValue is null)
                    return DbResponse.CreateResponse(ResponeFlag.NOTFOUND);
               
                staticValue.Value = command.Value;
                staticValue.Data = command.Data;
                staticValue.Description = command.Description;
                staticValue.AdditionalValue = command.AdditionalValue;

                var result = await _context.SaveChangesAsync();
                if (result > 0)
                    return DbResponse.CreateResponse(ResponeFlag.SUCCESS);
                return DbResponse.CreateResponse(ResponeFlag.FAILED);
            }
            catch
            {
                return DbResponse.CreateResponse(ResponeFlag.EXCEPTION);
            }
        }
        public async Task<DbResponse> DeleteStaticValue(int id)
        {
            try
            {
                var staticValueType = await _context.StaticValues.FindAsync(id);
                if (staticValueType is null)
                    return DbResponse.CreateResponse(ResponeFlag.NOTFOUND);

                _context.StaticValues.Remove(staticValueType);

                var result = await _context.SaveChangesAsync();
                if (result > 0)
                    return DbResponse.CreateResponse(ResponeFlag.SUCCESS);
                return DbResponse.CreateResponse(ResponeFlag.FAILED);
            }
            catch
            {
                return DbResponse.CreateResponse(ResponeFlag.EXCEPTION);
            }
        }


        // Get static values
        public async Task<List<DropDownModel>> GetCategoryByType(TransactionType transactionType)
        {
            var staticValueType = await _context.StaticValueTypes.FirstOrDefaultAsync(x => x.AdditionalValue == transactionType.GetEnumDisplayName());
            var dropDown = await _context.StaticValues
                                                 .AsNoTracking()
                                                 .Where(x => x.TypeId == staticValueType.SNO)
                                                 .Select(x => new DropDownModel
                                                 {
                                                     Id = x.Value,
                                                     Label = x.Data,
                                                 }).ToListAsync();
            return dropDown;
        }
    }
}
