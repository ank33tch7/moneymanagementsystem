﻿using DAL.Common;
using DAL.Models;
using DAL.Models.StaticValues;

namespace DAL.Persistance.Repositories.StaticValues
{
    public interface IStaticRespository
    {

        // stattic value types
        Task<List<StaticValueTypeVm>> GetStaticValueTypes();
        Task<List<StaticValueTypeForDropdownVm>> GetStaticValueTypeForDropdown();

        Task<DbResponse> CreateStaticValueType(CreateStaticValueTypeCommand command);
        Task<DbResponse> UpdateStaticValueType(UpdateStaticValueTypeCommand command);
        Task<DbResponse> DeleteStaticValueType(int id);


        // stattic values
        Task<List<StaticValueVm>> GetStaticValues();
        Task<List<StaticValueVm>> GetStaticValuesByTypeId(int typeId);

        Task<DbResponse> CreateStaticValue(CreateStaticValueCommand command);
        Task<DbResponse> UpdateStaticValue(UpdateStaticValueCommand command);
        Task<DbResponse> DeleteStaticValue(int id);

        Task<List<DropDownModel>> GetCategoryByType(TransactionType transactionType);
    }
}
