﻿using DAL.Entities;
using DAL.Models;
using DAL.Models.Expense;
using DAL.Models.Income;
using DAL.Persistance.DataContext;
using DAL.Persistance.SqlConnect;
using Microsoft.EntityFrameworkCore;

namespace DAL.Persistance.Repositories.Incomes
{
    public class IncomeRepository : IIncomeRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly ISQLHelper _sqlHelper;
        public IncomeRepository(ApplicationDbContext context, ISQLHelper sQLHelper)
        {
            _context = context;
            _sqlHelper = sQLHelper;
        }

        public async Task<List<IncomeVm>> GetIncomes()
        {
            //var incomeList = await _context.Incomes
            //                .Select(x => new IncomeVm
            //                {
            //                    Id = x.Id,
            //                    Amount = x.MTransaction.Amount,
            //                    Notes = x.Notes,
            //                    Source = x.Source,
            //                    CreatedDate = x.CreatedDate.ToString("yyyy-MM-dd"),
            //                    TransactionId = x.TransactionId
            //                }).ToListAsync();
            string incomeSql = "SELECT FORMAT(i.CreatedDate, 'yyyy-MM-dd') CreatedDate,t.Amount, sv.Data Source, i.Notes , t.Id TransactionId , sv.Value Name FROM Incomes i JOIN MTransactions t on i.TransactionId = t.Id JOIN StaticValues sv on i.Source = sv.Value";
            var incomeList = await _sqlHelper.LoadSQLDataListAsync<IncomeVm>(incomeSql);
            return incomeList;
        }

        public async Task<DbResponse> CreateIncome(CreateIncomeCommand command)
        {
            try
            {
                var mtransaction = new MTransaction
                {
                    Amount = command.Amount,
                    TransactionType = Common.TransactionType.Income
                };

                var income = new Income
                {
                    Notes = command.Notes,
                    Source = command.Source,
                };

                mtransaction.AddIncome(income);

                _context.MTransactions.Add(mtransaction);
                var result = await _context.SaveChangesAsync();
                if (result > 0)
                    return DbResponse.CreateResponse(ResponeFlag.SUCCESS);
                return DbResponse.CreateResponse(ResponeFlag.FAILED);
            }
            catch
            {
                return DbResponse.CreateResponse(ResponeFlag.EXCEPTION);
            }
        }

        public async Task<decimal> GetTotalIncomeAmount()
        {
            var totalIncomeSql = "SELECT ISNULL(SUM(t.Amount),0) TotalIncome FROM Incomes i WITH(NOLOCK) JOIN MTransactions t WITH(NOLOCK) ON i.TransactionId = t.Id";
            var totalIncome = await _sqlHelper.LoadSQLDataSingleAsync<decimal>(totalIncomeSql);
            return totalIncome;
        }
    }
}
