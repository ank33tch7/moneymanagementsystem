﻿using DAL.Models;
using DAL.Models.Income;

namespace DAL.Persistance.Repositories.Incomes
{
    public interface IIncomeRepository
    {
        Task<List<IncomeVm>> GetIncomes();
        Task<DbResponse> CreateIncome(CreateIncomeCommand command);
        Task<decimal> GetTotalIncomeAmount();
    }
}
