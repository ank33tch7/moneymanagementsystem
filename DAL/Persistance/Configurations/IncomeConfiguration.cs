﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.Persistance.Configurations
{
    public class IncomeConfiguration : IEntityTypeConfiguration<Income>
    {
        public void Configure(EntityTypeBuilder<Income> builder)
        {
            builder.HasKey(e => e.Id);

            //builder.Property(x => x.Amount)
            //     .IsRequired()
            //     .HasColumnType("decimal(18,4)");

            builder.Property(x => x.Notes)
                    .IsRequired()
                    .HasMaxLength(500);

            builder.Property(x => x.Source)
                    .IsRequired()
                    .HasMaxLength(100);

            builder.HasOne(x => x.MTransaction)
                  .WithMany(y => y.Incomes)
                  .HasForeignKey(x => x.TransactionId)
                  .IsRequired()
                  .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
