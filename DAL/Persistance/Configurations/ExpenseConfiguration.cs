﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.Persistance.Configurations
{
    public class ExpenseConfiguration : IEntityTypeConfiguration<Expense>
    {
        public void Configure(EntityTypeBuilder<Expense> builder)
        {
            builder.HasKey(e => e.Id);

            //builder.Property(x => x.Amount)
            //     .IsRequired()
            //     .HasColumnType("decimal(18,4)");

            builder.Property(x => x.Notes)
                    .IsRequired()
                    .HasMaxLength(500);

            builder.Property(x => x.Category)
                    .IsRequired()
                    .HasMaxLength(100);

            builder.HasOne(x => x.MTransaction)
                 .WithMany(y => y.Expenses)
                 .HasForeignKey(x => x.TransactionId)
                 .IsRequired()
                 .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
