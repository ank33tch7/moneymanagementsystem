﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Persistance.Configurations
{
    public class LoanConfiguration : IEntityTypeConfiguration<Loan>
    {
        public void Configure(EntityTypeBuilder<Loan> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Name)
                    .IsRequired()
                    .HasMaxLength(255);

            //builder.Property(x => x.Amount)
            //       .IsRequired()
            //       .HasColumnType("decimal(18,4)");

            builder.Property(x => x.InterestRate)
                  .HasColumnType("decimal(18,4)");

            //builder.Property(x => x.RepaymentTerms)
            //        .IsRequired()
            //        .HasMaxLength(500);

            builder.Property(x => x.Notes)
                  .IsRequired()
                  .HasMaxLength(500);

            builder.HasOne(x => x.MTransaction)
                   .WithMany(y => y.Loans)
                   .HasForeignKey(x => x.TransactionId)
                   .IsRequired()
                   .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
