﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.Persistance.Configurations
{
    public class MTransactionConfiguration : IEntityTypeConfiguration<MTransaction>
    {
        public void Configure(EntityTypeBuilder<MTransaction> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Amount)
                 .IsRequired()
                 .HasColumnType("decimal(18,4)");
        }
    }
}
