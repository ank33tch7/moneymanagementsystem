﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.Persistance.Configurations
{
    public class InvestmentConfigration : IEntityTypeConfiguration<Investment>
    {
        public void Configure(EntityTypeBuilder<Investment> builder)
        {
            builder.HasKey(x => x.Id);

            //builder.Property(x => x.Amount)
            //     .IsRequired()
            //     .HasColumnType("decimal(18,4)");

            builder.Property(x => x.InvestmentType).IsRequired().HasMaxLength(255);
            builder.Property(x => x.Notes).IsRequired().HasMaxLength(500);

            builder.Property(x => x.Notes)
                   .IsRequired()
                   .HasMaxLength(500);

            builder.HasOne(x => x.MTransaction)
                 .WithMany(y => y.Investments)
                 .HasForeignKey(x => x.TransactionId)
                 .IsRequired()
                 .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
