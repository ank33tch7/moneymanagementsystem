﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.Persistance.Configurations
{
    public class StaticValueTypeConfiguration : IEntityTypeConfiguration<StaticValueType>
    {
        public void Configure(EntityTypeBuilder<StaticValueType> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Name)
                   .IsRequired()
                   .HasMaxLength(150);

            builder.Property(x => x.Description)
                   .IsRequired()
                   .HasMaxLength(255);

            builder.Property(x => x.AdditionalValue)
                 .HasMaxLength(150);
        }
    }
}
