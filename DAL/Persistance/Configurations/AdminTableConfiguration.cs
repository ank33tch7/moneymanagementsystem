﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.Persistance.Configurations
{
    public class AdminTableConfiguration : IEntityTypeConfiguration<AdminTable>
    {
        public void Configure(EntityTypeBuilder<AdminTable> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.UserName)
                   .IsRequired()
                   .HasMaxLength(15);

            builder.Property(x => x.Password)
                    .IsRequired();

            builder.Property(x => x.FullName)
                   .IsRequired()
                   .HasMaxLength(100);

            builder.Property(x => x.Email)
                    .IsRequired()
                    .HasMaxLength(255);

            builder.Property(x => x.Phone)
                    .IsRequired()
                    .HasMaxLength(15);

        }
    }
}
