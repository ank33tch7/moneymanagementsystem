﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.Persistance.Configurations
{
    public class StaticValueConfiguration : IEntityTypeConfiguration<StaticValue>
    {
        public void Configure(EntityTypeBuilder<StaticValue> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Value)
                   .IsRequired()
                   .HasMaxLength(100);

            builder.Property(x => x.Data)
                  .IsRequired()
                  .HasMaxLength(100);

            builder.Property(x => x.Description)
                 .HasMaxLength(255);

            builder.Property(x => x.AdditionalValue)
                 .HasMaxLength(100);

            //builder.HasOne(x => x.StaticValueType)
            //       .WithMany(y => y.StaticValues)
            //       .IsRequired()
            //       .HasForeignKey(x => x.TypeId)
            //       .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
