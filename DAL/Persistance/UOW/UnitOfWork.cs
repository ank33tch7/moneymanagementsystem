﻿using DAL.Persistance.DataContext;
using DAL.Persistance.Repositories.AdminTables;
using DAL.Persistance.Repositories.Expenses;
using DAL.Persistance.Repositories.Incomes;
using DAL.Persistance.Repositories.Investments;
using DAL.Persistance.Repositories.Loans;
using DAL.Persistance.Repositories.MTransactions;
using DAL.Persistance.Repositories.StaticValues;
using DAL.Persistance.SqlConnect;

namespace DAL.Persistance.UOW
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        private readonly ISQLHelper _sqlHelper;
        public UnitOfWork(ApplicationDbContext context, ISQLHelper sQLHelper)
        {
            _context = context;
            _sqlHelper = sQLHelper;
        }
        public IAdminTableRepository _adminTable;
        public IStaticRespository _static;
        public IMTransactionRepository _transactions;
        public IExpenseRepository _expenses;
        public IIncomeRepository _incomes;
        public IInvestmentRepository _inventments;
        public ILoanRepository _loan;

        public IAdminTableRepository Users
        {
            get
            {
                if (_adminTable is null)
                    return new AdminTableRepository(_context);
                return _adminTable;
            }
        }

        public IStaticRespository Static
        {
            get
            {
                if (_static is null)
                    return new StaticRespository(_context, _sqlHelper);
                return _static;
            }
        }

        public IMTransactionRepository Transactions
        {
            get
            {
                if (_transactions is null)
                    return new MTransactionRepository(_context, _sqlHelper);
                return _transactions;
            }
        }

        public IExpenseRepository Expenses
        {
            get
            {
                if (_expenses is null)
                    return new ExpenseRepository(_context, _sqlHelper);
                return _expenses;
            }
        }

        public IIncomeRepository Incomes
        {
            get
            {
                if (_incomes is null)
                    return new IncomeRepository(_context, _sqlHelper);
                return _incomes;
            }
        }
        public IInvestmentRepository Investments
        {
            get
            {
                if (_inventments is null)
                    return new InvestmentRepository(_context, _sqlHelper);
                return _inventments;
            }
        }

        public ILoanRepository Loans
        {
            get
            {
                if (_loan is null)
                    return new LoanRepository(_context, _sqlHelper);
                return _loan;
            }
        }
    }
}
