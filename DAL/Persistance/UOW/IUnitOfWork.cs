﻿using DAL.Persistance.Repositories.AdminTables;
using DAL.Persistance.Repositories.Expenses;
using DAL.Persistance.Repositories.Incomes;
using DAL.Persistance.Repositories.Investments;
using DAL.Persistance.Repositories.Loans;
using DAL.Persistance.Repositories.MTransactions;
using DAL.Persistance.Repositories.StaticValues;

namespace DAL.Persistance.UOW
{
    public  interface IUnitOfWork
    {
        IAdminTableRepository Users { get; }
        IStaticRespository Static { get; }
        IMTransactionRepository Transactions { get; } 
        IExpenseRepository Expenses { get; }
        IIncomeRepository Incomes { get; }
        IInvestmentRepository Investments { get; }
        ILoanRepository Loans { get; }
    }
}
