﻿using Dapper;
using System.Data;

namespace DAL.Persistance.SqlConnect
{
    public interface ISQLHelper
    {
        T LoadSPDataSingleWithParam<T>(string sql, DynamicParameters param);
        T LoadSPDataSingleWithoutParam<T>(string sql);
        List<T> LoadSPDataListWithParam<T>(string sql, DynamicParameters param);
        T LoadSQLDataSingle<T>(string sql);
        List<T> LoadSQLDataList<T>(string sql);
        List<T> LoadSQLDataListWithParam<T>(string sql, DynamicParameters p);
        T LoadSQLDataSingleWithParam<T>(string sql, DynamicParameters p);
        DataTable ExecuteDataTable(string sql);
        int RunSQLWithParam(string sql, DynamicParameters p);

        // async methods.
        Task<T> LoadSPDataSingleWithoutParamAsync<T>(string sql);
        Task<T> LoadSPDataSingleWithParamAsync<T>(string sql, DynamicParameters param);
        Task<List<T>> LoadSPDataListWithParamAsync<T>(string sql, DynamicParameters param);
        Task<T> LoadSQLDataSingleAsync<T>(string sql);
        Task<List<T>> LoadSQLDataListAsync<T>(string sql);
        Task<List<T>> LoadSQLDataListWithParamAsync<T>(string sql, DynamicParameters p);
        Task<T> LoadSQLDataSingleWithParamAsync<T>(string sql, DynamicParameters p);
        Task<int> RunSQLWithParamAsync(string sql, DynamicParameters p);
    }
}
