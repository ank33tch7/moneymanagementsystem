﻿using Dapper;
using Newtonsoft.Json.Linq;
using System.Data;
using System.Data.SqlClient;

namespace DAL.Persistance.SqlConnect
{
    public  class SQLHelper : ISQLHelper
    {
        private SqlConnection _connection;
        public SQLHelper()
        {
            Init();
        }

        private void Init()
        {
            _connection = new SqlConnection(GetConnectionString());
        }

        private string GetConnectionString()
        {
            var targetPath = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), @"..\MMSApi"));
            string appSettingsPath = Path.Combine(targetPath, "appsettings.json");
            string json = File.ReadAllText(appSettingsPath);
            var jObject = JObject.Parse(json);
            string connectionString = jObject.GetValue("ConnectionStrings")["DefaultConnection"].ToString();
            return connectionString;
        }

        public T LoadSPDataSingleWithParam<T>(string sql, DynamicParameters param)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnectionString()))
            {
                var result = cnn.Query<T>(sql, param, commandType: CommandType.StoredProcedure).SingleOrDefault();
                return result;
            }
        }

        public T LoadSPDataSingleWithoutParam<T>(string sql)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnectionString()))
            {
                var result = cnn.Query<T>(sql, commandType: CommandType.StoredProcedure).SingleOrDefault();
                return result;
            }
        }
        public List<T> LoadSPDataListWithParam<T>(string sql, DynamicParameters param)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnectionString()))
            {
                var result = cnn.Query<T>(sql, param, commandType: CommandType.StoredProcedure).ToList();
                return result;
            }
        }
        public T LoadSQLDataSingle<T>(string sql)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnectionString()))
            {
                return cnn.Query<T>(sql).SingleOrDefault();
            }
        }
        public List<T> LoadSQLDataList<T>(string sql)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnectionString()))
            {
                var result = cnn.Query<T>(sql).ToList();
                return result;
            }
        }
        public List<T> LoadSQLDataListWithParam<T>(string sql, DynamicParameters p)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnectionString()))
            {
                return cnn.Query<T>(sql, p).ToList();
            }
        }
        public T LoadSQLDataSingleWithParam<T>(string sql, DynamicParameters p)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnectionString()))
            {
                return cnn.Query<T>(sql, p).SingleOrDefault();
            }
        }
        public DataTable ExecuteDataTable(string sql)
        {
            using (SqlConnection con = new SqlConnection(GetConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        return dt;
                    }
                }
            }
        }
        public int RunSQLWithParam(string sql, DynamicParameters p)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnectionString()))
            {
                return cnn.Execute(sql, p);
            }
        }


        // Aync Methods
        public async Task<T> LoadSPDataSingleWithoutParamAsync<T>(string sql)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnectionString()))
            {
                var result = await cnn.QueryAsync<T>(sql, commandType: CommandType.StoredProcedure);
                return result.FirstOrDefault();
            }
        }
        public async Task<T> LoadSPDataSingleWithParamAsync<T>(string sql, DynamicParameters param)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnectionString()))
            {
                var result = await cnn.QueryAsync<T>(sql, param, commandType: CommandType.StoredProcedure);
                return result.SingleOrDefault();
            }
        }
        public async Task<List<T>> LoadSPDataListWithParamAsync<T>(string sql, DynamicParameters param)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnectionString()))
            {
                var result = await cnn.QueryAsync<T>(sql, param, commandType: CommandType.StoredProcedure);
                return result.ToList();
            }
        }
        public async Task<T> LoadSQLDataSingleAsync<T>(string sql)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnectionString()))
            {
                var result = await cnn.QueryAsync<T>(sql);
                return result.SingleOrDefault();
            }
        }
        public async Task<List<T>> LoadSQLDataListAsync<T>(string sql)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnectionString()))
            {
                var result = await cnn.QueryAsync<T>(sql);
                return result.ToList();
            }
        }
        public async Task<List<T>> LoadSQLDataListWithParamAsync<T>(string sql, DynamicParameters p)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnectionString()))
            {
                var result = await cnn.QueryAsync<T>(sql, p);
                return result.ToList();
            }
        }
        public async Task<T> LoadSQLDataSingleWithParamAsync<T>(string sql, DynamicParameters p)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnectionString()))
            {
                var result = await cnn.QueryAsync<T>(sql, p);
                return result.SingleOrDefault();
            }
        }
        public async Task<int> RunSQLWithParamAsync(string sql, DynamicParameters p)
        {
            using (IDbConnection cnn = new SqlConnection(GetConnectionString()))
            {
                return await cnn.ExecuteAsync(sql, p);
            }
        }
    }
}
