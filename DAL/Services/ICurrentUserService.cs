﻿namespace DAL.Services
{
    public interface ICurrentUserService
    {
        public string UserId { get; }
        public string FullName { get; }
        public string UserName { get; }
        public string Phone { get; }
        public string Email { get; }
    }
}
