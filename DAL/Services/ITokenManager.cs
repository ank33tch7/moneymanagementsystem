﻿using DAL.Models.AdminTable;
using System.IdentityModel.Tokens.Jwt;

namespace DAL.Services
{
    public interface ITokenManager
    {
        public JwtSecurityToken GetToken(TokenModel user);
    }
}
